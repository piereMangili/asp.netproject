using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using MyCourse.Models.Services.Application;
using MyCourse.Models.ViewModels;

namespace MyCourse.Controllers
{
    public class CorsiController : Controller
    {
        private readonly ICorsiService corsiService;

        public CorsiController(ICorsiService corsiService)
        {
            this.corsiService = corsiService;
        }
        public IActionResult Index()
        {
            ViewData["Title"] = "Catalogo dei corsi";
            List<CorsiViewModel> corsi = corsiService.GetCorsi();
            return View(corsi);
        }

        public IActionResult Detail(int id)
        {
            CorsiDetailViewModel viewModel = corsiService.GetCorso(id);
            ViewData["Title"] = viewModel.Title;
            return View(viewModel);
        }
    }
}