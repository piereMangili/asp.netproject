using System.Collections.Generic;
using MyCourse.Models.ViewModels;

namespace MyCourse.Models.Services.Application
{
    public interface ICorsiService
    {
         List<CorsiViewModel> GetCorsi();

         CorsiDetailViewModel GetCorso(int id);
    }
}